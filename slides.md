---
revealOptions:
    transition: 'fade'
---
![bpass](/images/logo.png)
# bpass.
Blockchain password storage - made simple
---
![muhi](/images/muhi.JPG)
## **Chief Executive Officer and Head of Management**
----
# ●●●●●●●
---
![daniel](/images/daniel.JPG)
## **Senior Software Engineer, Chief Technology Officer and Head of Blockchain Research**
----
![lock](/images/lock.png)
----
###### 98%
# Investor Satisfaction
----
# One more thing.
---
![lena](/images/lena.JPG)
## **Head of Applied Cryptography and Senior Front-End Developer**
---
![leia](/images/leia.JPG)
## **Assistant Cryptography Researcher and Senior Marketing Manager**
----
###### Testimonials
----
# "I invested **10** cents. I got **20 cents out of it**. __I love it__!" - David, Investor and blockchain enthusiast
---
![jole](/images/jole.JPG)
## **Vice President and Head of Hardware Engineering**
